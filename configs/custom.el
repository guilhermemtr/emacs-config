
;; delete not kill it into kill-ring
;; _based on_ https://github.com/c02y/dotemacs.d/blob/master/init.el#L656

(defun delete-word (arg)
  "Delete characters forward until encountering the end of a word.
With argument, do this that many times.
This command does not push text to `kill-ring'."
  (interactive "p")
  (delete-region
   (point)
   (progn
     (forward-word arg)
     (point))))

(defun delete-word-backward (arg)
  "Delete(not kill) characters backward until encountering the beginning of the syntax-subword.
With argument, do this that many times."
  (interactive "p")
  (delete-word (- arg)))

(defun delete-line-to-end (arg)
  "Delete text from current position to end of line char.
With argument, forward ARG lines."
  (interactive "p")
  (let (x1 x2)
    (setq x1 (point))
    (if (eolp) (forward-line arg) (forward-line (- arg 1)))
    (move-end-of-line 1)
    (setq x2 (point))
    (delete-region x1 x2)
    (when (bolp) (delete-char 1))))

(defun delete-line-backward (arg)
  "Delete text between the beginning of the line to the cursor position.
With argument, backward ARG lines."
  (interactive "p")
  (let (x1 x2)
    (setq x1 (point))
    (if (bolp) (forward-line (- arg)) (forward-line (- 1 arg)))
    (move-beginning-of-line 1)
    (setq x2 (point))
    (delete-region x1 x2)))
(bind-keys*
 ("M-d" . delete-word)
 ("<M-backspace>" . delete-word-backward)
 ("C-k" . delete-line-to-end)
 ("C-c k" . delete-line-backward))
