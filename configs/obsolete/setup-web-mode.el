

(defun web-mode-setup ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 2)
  (setq web-mode-style-padding 2)
  (setq web-mode-script-padding 2)
  (setq web-mode-block-padding 2)
  (define-key web-mode-map (kbd "C-n") 'web-mode-tag-match)
  (setq web-mode-enable-auto-pairing 0)
  (setq web-mode-enable-css-colorization t)
  (setq web-mode-enable-block-face t)
  (setq web-mode-enable-part-face t)
  (setq web-mode-enable-comment-keywords t)
  (setq web-mode-enable-current-element-highlight t)
  (setq web-mode-enable-current-column-highlight t)
  )

(use-package web-mode
  :defer t
  :config (progn
	    (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
	    (add-to-list 'auto-mode-alist '("\\.css?\\'" . web-mode))
	    (add-to-list 'auto-mode-alist '("\\.js?\\'" . web-mode))
	    (add-to-list 'auto-mode-alist '("\\.json?\\'" . web-mode))
	    (add-hook 'web-mode-hook 'web-mode-setup)))


(add-to-list 'company-backends 'company-web-html)
(add-to-list 'company-backends 'company-web-jade)
(add-to-list 'company-backends 'company-web-slim)

(provide 'setup-web-mode)
