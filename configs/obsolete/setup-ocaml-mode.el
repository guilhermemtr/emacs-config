(add-hook 'tuareg-mode-hook #'merlin-mode)
;;(add-hook 'caml-mode-hook 'merlin-mode)


;; Make company aware of merlin
(with-eval-after-load 'company
  (add-to-list 'company-backends 'merlin-company-backend))
;; Enable company on merlin managed buffers
(add-hook 'merlin-mode-hook 'company-mode)
;; Or enable it globally:
;; (add-hook 'after-init-hook 'global-company-mode)




;; flycheck ocaml
(with-eval-after-load 'merlin
  ;; Disable Merlin's own error checking
  (setq merlin-error-after-save nil)
  
  ;; Enable Flycheck checker
  (flycheck-ocaml-setup))



(let ((opam-share (ignore-errors (car (process-lines "opam" "config" "var" "share")))))
  (when (and opam-share (file-directory-p opam-share))
    ;; Register Merlin
    (add-to-list 'load-path (expand-file-name "emacs/site-lisp" opam-share))
    (autoload 'merlin-mode "merlin" nil t nil)
    ;; Automatically start it in OCaml buffers
    (add-hook 'tuareg-mode-hook 'merlin-mode t)
    (add-hook 'caml-mode-hook 'merlin-mode t)
    ;; Use opam switch to lookup ocamlmerlin binary
    (setq merlin-command 'opam)))


(provide 'setup-ocaml-mode)
