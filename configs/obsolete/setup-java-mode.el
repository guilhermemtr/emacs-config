(use-package eclim
  :defer t
  :config
  (global-eclim-mode)) 

(use-package company-emacs-eclim
  :defer t
  :config (company-emacs-eclim-setup))


(provide 'setup-java-mode)
