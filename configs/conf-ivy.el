
(global-unset-key (kbd "C-b"))              ;; old Move a char backward


(use-package ivy
  :init (progn 
	  (setq ivy-use-virtual-buffers t)
	  (setq ivy-count-format "[%d/%d]: ")
	  (setq ivy-current-match t)
	  (setq ivy-minibuffer-match-face-1 t)
	  (setq ivy-confirm-face t)
	  (setq ivy-virtual t)
	  (setq ivy-match-required-face t)
	  (setq ivy-subdir t)
	  (setq ivy-remote t)
          (setq ivy-format-function 'ivy-format-function-line)
          (setq ivy-extra-directories nil)
          (ivy-mode +1))
  :bind (("C-k" . kill-this-buffer)
         ("C-S-b" . ibuffer-list-buffers)
         ("C-b" . ivy-switch-buffer))
  :config (progn (ivy-mode t)))

(use-package ivy-prescient
  :after counsel
  :config
  (ivy-prescient-mode 1))


(use-package ivy-rich
  :hook (ivy-mode . ivy-rich-mode)
  :config (progn
            (ivy-rich-mode 1)
            (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line)))


(use-package all-the-icons)

(use-package all-the-icons-ivy-rich
  :init (progn (all-the-icons-ivy-rich-mode)
               (setq all-the-icons-ivy-rich-icon-size 1.0))
  :requires (all-the-icons))


(use-package swiper
  ;; I think I won't use this regularly.
  ;; (global-set-key (kbd "C-S-s")   'swiper-mc)
  )


(global-unset-key (kbd "M-x"))
(global-unset-key (kbd "C-s"))
(global-unset-key (kbd "C-S-s"))
(global-unset-key (kbd "M-y"))
(global-unset-key (kbd "C-x C-f"))
(global-unset-key (kbd "C-f"))
(global-unset-key (kbd "C-c g"))
(global-unset-key (kbd "C-c j"))
(global-unset-key (kbd "C-l"))

(use-package counsel
;;  :config (progn
;;            (setcdr (assoc 'counsel-M-x ivy-initial-inputs-alist) ""))
  :bind
  (("M-x"           . counsel-M-x)
   ("C-s"           . counsel-grep-or-swiper)
   ("C-S-s"         . counsel-grep-or-swiper-backward)
   ("C-r"           . replace-string)
   ("M-y"           . counsel-yank-pop)
   ("C-x C-f"       . counsel-find-file)
   ("C-f"           . counsel-ag)

   ("C-c g"         . counsel-git)
   ("C-c j"         . counsel-git-grep)
   ("C-l"           . counsel-locate)
   ;; I don't use this regularly anyway.
   ;; (global-set-key (kbd "M-S-X")   'counsel-descbinds)
   
   
   ;;("<f1> f"        . counsel-describe-function)
   ;;("<f1> v"        . counsel-describe-variable)
   ;;("<f1> l"        . counsel-find-library)


   ;;("<f2> i"        . counsel-info-lookup-symbol)
   ;;("<f2> u"        . counsel-unicode-char)
   ))

(provide 'conf-ivy)
