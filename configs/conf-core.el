;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;
;;;;;;;;;;
;;;;;;;;;;   core emacs configurations
;;;;;;;;;;
;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; use-package
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; install packages when absent by default
(use-package use-package-ensure
  :config
  (setq use-package-always-ensure t))


;; update packages
(use-package auto-package-update
  :config
  (setq auto-package-update-delete-old-versions t)
  (setq auto-package-update-hide-results t)
  (auto-package-update-maybe))

(use-package async)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; packages that were missing in configuration
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package dash)
(use-package compat)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; session manager (save current workspace, etc)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Setup desktop session management shortcuts
(use-package desktop
  :ensure t
  :diminish (desktop-save-mode . ""))

(use-package saveplace
  :ensure f
  :config (progn
            (save-place-mode 1) 
            (setq-default save-place t)
            (setq save-place-file (locate-user-emacs-file "places" ".emacs-places"))
            (setq save-place-forget-unreadable-files nil)))

;; define function to shutdown emacs server instance
(defun server-shutdown ()
  "Save buffers, Quit, and Shutdown (kill) server"
  (interactive)
  (save-some-buffers)
  (kill-emacs))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; package manager
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package paradox                    ; Better package menu
  :ensure t
  ;;:bind (("C-c p l" . paradox-list-packages)
  ;;	 ("C-c p u" . paradox-upgrade-packages))
  ;;:config
  ;;((paradox-execute-asynchronously nil)  ; No async update, please
  ;; (paradox-spinner-type 'moon)          ; Fancy spinner
  ;; (paradox-display-download-count t)    ; Show all possible counts
  ;; (paradox-display-star-count t)        ; Show number of stars
  ;; (paradox-automatically-star nil))     ; Don't star automatically
  )

;; token for paradox (from my github)
(setq paradox-github-token "ec9d780a0a691217c6617f1e6e7d0977c54de3f1")

(use-package spinner)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; utils
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package sudo-edit)

;; disable autosave
(setq auto-save-default nil)

(use-package keycast
  :commands keycast-mode)

;; recent files - do I use this?
;;(use-package recentf
;;  :config
;;  (progn (recentf-mode 1)
;;         (setq recentf-max-menu-items 50))
;;  :bind ("C-o" . counsel-recentf))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; personal variables
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq user-mail-address "guilhermemtr@gmail.com")
(setq user-name "Guilherme Miguel Teixeira Rito")
(setq user-signature "Guilherme Rito")

;; set locale
;; (set-language-environment "Latin-9")



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; ui
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; inhibit emacs startup buffer
(setq inhibit-startup-screen t)

;; hide top bars
(menu-bar-mode -1)
(tool-bar-mode -1)

;; hide scroll bar
;; (toggle-scroll-bar -1)
(scroll-bar-mode -1)

;; show column nr.
(setq column-number-mode t)

(setq ring-bell-function 'ignore)

(fset 'yes-or-no-p 'y-or-n-p)

(use-package command-log-mode)

;; theme
;; this is needed for emacsclient to load the theme.
;;(if (daemonp)
;;    (add-hook 'after-make-frame-functions
;;              (lambda (frame)
;;                (with-selected-frame frame
;;                  (load-theme 'nord t))))
;;  (load-theme 'nord t))
(use-package color-theme-sanityinc-tomorrow
  :config (color-theme-sanityinc-tomorrow-day))

;;(use-package theme-changer ;; change-theme at night
;;  :init ;; sunrise/sunset theme changer
;;  (setq
;;   calendar-location-name "Lisbon, PT"
;;   calendar-latitude 38.72
;;   calendar-longitude 9.14)
;;
;;  :config ;; cycle through day and night sanityinc-tomorrow themes
;;  (change-theme 'sanityinc-tomorrow-day 'sanityinc-tomorrow-night)
;;  )

;; smoother scrolling
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)) ;; one line at a time
      mouse-wheel-progressive-speed nil ;; don't accelerate scrolling
      mouse-wheel-follow-mouse 't ;; scroll window under mouse
      auto-window-vscroll nil
      scroll-step 1 ;; keyboard scroll one line at a time
      scroll-margin 1
      ;;(setq scroll-conservatively 10000)
      scroll-conservatively 0
      scroll-up-aggressively 0.01
      scroll-down-aggressively 0.01)
(setq-default scroll-up-aggressively 0.01
	      scroll-down-aggressively 0.01)

;; better way to distinguish buffers for files with equal filename
(use-package uniquify
  :ensure f
  :config (setq uniquify-buffer-name-style 'forward))

;; loads the mode-line
;;(use-package powerline
;;  :init
;;  (add-to-list 'load-path (concat config-path "emacs-powerline")))

(use-package prescient)

(use-package auto-dim-other-buffers)

;; multiple cursors
(use-package multiple-cursors
  :bind (("C-c SPC"           . set-rectangular-region-anchor)
         ("C-c C-SPC"         . set-rectangular-region-anchor)
	 ("C-<down-mouse-1>"  . mc/add-cursor-on-click)
         ("C-c C-m"           . mc/mark-more-like-this-extended)         
         ("C-c C-<right>"     . mc/mark-next-word-like-this)
         ("C-c C-<left>"      . mc/mark-previous-word-like-this)
         ("C-c <return>"      . mc/mark-all-words-like-this)
         ("C-c C-<return>"    . mc/mark-all-words-like-this)))
;;("M-<down>" . mc/mark-next-like-this)
;;("M-<up>" . mc/mark-previous-like-this)))

;; jump to characters
(use-package avy
  :config (progn (setq avy-keys (number-sequence ?a ?z))
		 (setq avy-background t)
		 (setq avy-case-fold-search nil)
		 (setq avy-highlight-first t))
  
  :bind (("C-z"                 . avy-goto-char)))
;;	 ("C-<return>"          . avy-goto-word-1)))


;; https://www.emacswiki.org/emacs/UndoTree
;; Undo tree
(use-package undo-tree
  :diminish undo-tree-mode
  :init
  (global-undo-tree-mode 1)
  ;;(defalias 'redo 'undo-tree-redo)
  :bind (("C-_"   . undo)
         ("M-_"   . redo)
         ("C-x u" . undo-tree-visualize))
  :config (progn (setq undo-tree-auto-save-history nil))
  :diminish undo-tree-mode)


(provide 'conf-core)
