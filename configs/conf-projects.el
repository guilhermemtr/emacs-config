;;(use-package magit
;;  :config (progn (setq magit-completing-read-function 'ivy-completing-read)))

;;(use-package sr-speedbar
;;  :bind (("<f9>" . sr-speedbar-toggle))
;;  :config (progn (sr-speedbar-refresh-turn-on)))


(use-package projectile
  :bind (("M-f" . projectile-ag)
	 ("M-r" . projectile-replace)
         ("M-o" . projectile-find-file))
  :config (progn (setq projectile-completion-system 'ivy)))


(provide 'conf-projects)
