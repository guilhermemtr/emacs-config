(use-package function-args
  :config (progn
	    (setq-local imenu-create-index-function #'moo-local-jump)))


(defun c-mode-clang-format-keybinds ()
  "For use in `c-mode-hook' or `c++-mode-hook'."
  (local-set-key (kbd "<f12>") 'clang-format-buffer))

(use-package clang-format
  :functions clang-format-buffer
  :hook (c-mode c++-mode) . 'c-mode-clang-format-keybinds)

(use-package cmake-mode
  :diminish cmake-mode 
  :config (progn 
            (company-quickhelp-mode t)
            (global-company-mode t)
            (setq company-idle-delay 0.2)))


(use-package flycheck)

(use-package flycheck-clang-analyzer)

(use-package flycheck-rtags)

(use-package company-c-headers
  :defer t
  :hook ((c-mode c++-mode) . flycheck-mode)
  :config (progn
	    ;(add-to-list 'company-c-headers-path-system "/usr/include/c++/")
	    (add-to-list 'company-backends 'company-c-headers)))

(provide 'conf-c)
