

(use-package auctex
  :bind (("C-<return>" . TeX-view)
         ("<f5>" . TeX-command-run-all)))




(use-package company-auctex
  :config (company-auctex-init))

(use-package company-math
  :hook (Tex-mode . (lambda () (setq-local company-backends
                                           (append
                                            '((company-math-symbols-latex company-latex-commands))
                                            company-backends))))
  )

(use-package company-reftex)


(use-package reftex
  :config
  (setq reftex-cite-prompt-optional-args t)
  (reftex-mode 1)
  (setq reftex-plug-into-AUCTeX t)
  (setq reftex-format-cite-function 
        '(lambda (key fmt)
	   (let ((cite (replace-regexp-in-string "%l" key fmt)))
	     (if (or (= ?~ (string-to-char fmt))
		     (member (preceding-char) '(?\ ?\t ?\n ?~)))
	         cite
	       (concat "~" cite)))))) ;; Prompt for empty optional arguments in cite

(use-package company-reftex
  :config (progn (add-to-list 'company-backends 'company-reftex-labels)
                 (add-to-list 'company-backends 'company-reftex-citations)))


(with-eval-after-load "tex"
  ;; enable synctex support for latex-mode
  (add-hook 'LaTeX-mode-hook 'TeX-source-correlate-mode)
  ;; add a new view program
  (add-to-list 'TeX-view-program-list
               '(;; arbitrary name for this view program
                 "Zathura"
                 (;; zathura command (may need an absolute path)
                  "zathura"
                  ;; %o expands to the name of the output file
                  " %o"
                  ;; insert page number if TeX-source-correlate-mode
                  ;; is enabled
                  (mode-io-correlate " --synctex-forward %n:0:%b"))))
  ;; was latexmk -pdf
  (setq TeX-command "latexmk -pdflatex='pdflatex -file-line-error -synctex=1' -pvc")

  ;; use the view command named "Zathura" for pdf output
  (setcdr (assq 'output-pdf TeX-view-program-selection) '("Zathura")))

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
(setq TeX-PDF-mode t)


(provide 'conf-latex)
