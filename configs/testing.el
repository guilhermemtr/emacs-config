;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
;;         testing emacs configurations
;;                 first test if new emacs packages are useful before moving
;;                 them to the set of definitive packages for my emacs setup
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(global-unset-key (kbd "C-m"))              ;; old Add line?

(use-package focus
  :config
  (progn 
    '((prog-mode . defun) (text-mode . sentence)))
  :bind ("C-m" . focus-mode))


(use-package solaire-mode
  :hook (after-init . solaire-global-mode))

(setq default-minibuffer-frame
      (make-frame
       '((name . "minibuffer")
         (width . 0)
         (height . 0)
         (minibuffer . only)
         (top . 0)
         (left . 0)
         )))
(setq new-frame
      (make-frame
       '((name . "editor")
         (width . 80)
         (height . 30)
         (minibuffer . nil)
         (top . 50)
         (left . 0)
         )))


(provide 'testing)
