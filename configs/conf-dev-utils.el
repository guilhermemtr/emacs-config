;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
;;         core dev configurations
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package ag)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; typing suggestions
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package pos-tip)
(use-package company-quickhelp
  :requires (pos-tip))

(use-package company
  :diminish company-mode 
  :config (progn 
	    (company-quickhelp-mode t)
	    (global-company-mode t)
            (setq company-minimum-prefix-length 1)
            (setq company-selection-wrap-around t)
	    (setq company-idle-delay 0.2)
            (setq company-dabbrev-downcase nil)))

(use-package company-prescient
  :after company
  :config (company-prescient-mode 1))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; text editing
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq-default sentence-end-double-space nil)
(setq require-final-newline t)

(setq tab-width 2)
(setq-default fill-column 80)
(setq make-backup-files nil)

(add-hook 'text-mode-hook #'auto-fill-mode)
(add-hook 'prog-mode-hook #'auto-fill-mode)

(add-hook 'text-mode-hook #'visual-line-mode)
(add-hook 'prog-mode-hook #'visual-line-mode)

(show-paren-mode 1)
(setq show-paren-delay 0)

(global-unset-key (kbd "M-z"))

;; correct errors
(use-package flyspell
  :diminish flyspell-mode
  :hook ((text-mode . flyspell-mode)
	 (text-mode . flyspell-buffer)
	 (prog-mode . flyspell-prog-mode)
         (c-mode . flyspell-prog-mode)
         (c++-mode . flyspell-prog-mode)))


(delete-selection-mode 1)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; set key binds
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; setup bindings for sorting
;; (global-set-key (kbd "C-c s l") 'sort-lines)
;; (global-set-key (kbd "C-c s p") 'sort-pages)
;; (global-set-key (kbd "C-c s f") 'sort-fields)
;; (global-set-key (kbd "C-c s c") 'sort-columns)
;; (global-set-key (kbd "C-c s n") 'sort-numeric-fields)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; indentation
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq-default indent-tabs-mode nil)

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package indent-guide
  :diminish indent-guide-mode
  :hook (prog-mode . indent-guide-mode)
  :config (progn
	    ;; (indent-guide-global-mode)
	    (set-face-background 'indent-guide-face "dimgray")
	    (setq indent-guide-char "%")))
;;	    (defun set-newline-and-indent ()
;;	      (local-set-key (kbd "RET") 'newline-and-indent))
;;	    (add-hook 'prog-mode-hook 'set-newline-and-indent)))

(use-package aggressive-indent
  :diminish aggressive-indent-mode
  :hook ((emacs-lisp-mode css-mode tuareg-mode) . aggressive-indent-mode))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; snippets
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package yasnippet
  :diminish yas-global-mode
  :config (progn
	    (yas-global-mode t)
	    (company-yasnippet t)
            
            (defvar company-mode/enable-yas t
              "Enable yasnippet for all backends.")

            (defun company-mode/backend-with-yas (backend)
              (if (or (not company-mode/enable-yas) (and (listp backend) (member 'company-yasnippet backend)))
                  backend
                (append (if (consp backend) backend (list backend))
                        '(:with company-yasnippet))))
            
            (setq company-backends (mapcar #'company-mode/backend-with-yas company-backends))))


(use-package counsel-etags)


(provide 'conf-dev-utils)
