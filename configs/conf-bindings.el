;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; unbind key binds
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(global-unset-key (kbd "C-f"))              ;; old Move one char forward
(global-unset-key (kbd "M-f"))              ;; old Move a word forward
(global-unset-key (kbd "C-b"))              ;; old Move a char backward
(global-unset-key (kbd "M-b"))              ;; old Move a word backward
;; (global-unset-key (kbd "C-a"))           ;; old Go to the beginning of a line
;; (global-unset-key (kbd "C-e"))           ;; old Go to the end of a line
(global-unset-key (kbd "C-d"))              ;; old Delete a char
(global-unset-key (kbd "M-d"))              ;; old Delete a word
(global-unset-key (kbd "C-p"))              ;; old Move to the previous line
(global-unset-key (kbd "C-n"))              ;; old Move to the next line
(global-unset-key (kbd "C-v"))              ;; old Page down
(global-unset-key (kbd "M-v"))              ;; old Page up
(global-unset-key (kbd "C-l"))              ;; old Center the screen
(global-unset-key (kbd "M-u"))              ;; old Upper case
(global-unset-key (kbd "M-l"))              ;; old Lower case
(global-unset-key (kbd "M-c"))              ;; old Capitalize
(global-unset-key (kbd "C-s"))              ;; old Search forward
(global-unset-key (kbd "C-r"))              ;; old Search reverse
(global-unset-key (kbd "C-x c"))            ;; old Helm something
(global-unset-key (kbd "C-z"))              ;; old minimize emacs
;;(global-unset-key (kbd "C-z"))            ;; old minimize emacs
(global-unset-key (kbd "C-x C-v"))          ;; old close buffer and open new file
(global-unset-key (kbd "C-k"))              ;; old kill line
(global-unset-key (kbd "C-@"))              ;; old kill line
(global-unset-key (kbd "C-j"))              ;; electric-newline-and-maybe-indent

;;(global-unset-key (kbd "RET"))              ;; newline
(global-unset-key (kbd "C-o"))              ;; open-line
(global-unset-key (kbd "C-q"))              ;; quoted-insert
(global-unset-key (kbd "C-t"))              ;; transpose-chars
(global-unset-key (kbd "<f2> 2"))           ;; 2C-two-columns
(global-unset-key (kbd "<f2> b"))           ;; 2C-associate-buffer
(global-unset-key (kbd "<f2> s"))           ;; 2C-split
(global-unset-key (kbd "<f2> <f2>"))        ;; 2C-two-columns
(global-unset-key (kbd "C-x 6 2"))          ;; 2C-two-columns
(global-unset-key (kbd "C-x 6 b"))          ;; 2C-associate-buffer
(global-unset-key (kbd "C-x 6 s"))          ;; 2C-split
(global-unset-key (kbd "C-x 6 <f2>"))       ;; 2C-two-columns






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; unbind mouse key binds
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(global-unset-key (kbd "C-<mouse-1>"))           ;; unknown mouse button assignment
(global-unset-key (kbd "M-<mouse-1>"))           ;; unknown mouse button assignment
(global-unset-key (kbd "C-<down-mouse-1>"))      ;; unknown mouse button assignment
(global-unset-key (kbd "M-<down-mouse-1>"))      ;; unknown mouse button assignment

(global-unset-key (kbd "C-<mouse-2>"))           ;; unknown mouse button assignment
(global-unset-key (kbd "M-<mouse-2>"))           ;; unknown mouse button assignment

(global-unset-key (kbd "C-<mouse-3>"))           ;; unknown mouse button assignment
(global-unset-key (kbd "M-<mouse-3>"))           ;; unknown mouse button assignment


(global-unset-key (kbd "C-<mouse-8>"))           ;; unknown mouse button assignment
(global-unset-key (kbd "M-<mouse-8>"))           ;; unknown mouse button assignment

(global-unset-key (kbd "C-<mouse-9>"))           ;; unknown mouse button assignment
(global-unset-key (kbd "M-<mouse-9>"))           ;; unknown mouse button assignment




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; unbind minor mode key binds
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(global-unset-key (kbd "C-c C-q"))              ;; old aggressive-indent

(global-unset-key (kbd "C-,"))                  ;; old flyspell
(global-unset-key (kbd "C-."))                  ;; old flyspell
(global-unset-key (kbd "C-;"))                  ;; old flyspell
(global-unset-key (kbd "C-c $"))                ;; old flyspell
(global-unset-key (kbd "C-M-i"))                ;; old flyspell


(global-unset-key (kbd "C-c C-n"))               ;; old yas-new-snippet
(global-unset-key (kbd "C-c C-s"))               ;; old yas-insert-snippet
(global-unset-key (kbd "C-c C-v"))               ;; old yas-visit-snippet-file


(global-unset-key (kbd "C-/"))                   ;; undo-tree-undo
(global-unset-key (kbd "C-?"))                   ;; undo-tree-redo
(global-unset-key (kbd "C-x r U"))		 ;; undo-tree-restore-state-from-register
(global-unset-key (kbd "C-x r u"))		 ;; undo-tree-save-state-to-register




(provide 'conf-bindings)
